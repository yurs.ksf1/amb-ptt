from django.conf.urls import url
from . import views

from .views import *


urlpatterns = [
    url(r'^$', views.home, name='home3'),
    url(r'test/', views.listAct, name='vistaYs1'),
    url(r'test1/', views.listAct1, name='vistaYs'),
    url(r'test2/', views.MyAct, name='MyAct'),
    #url(r'MyModel/add/$', MyModelCreate.as_view(), name='MyModel-add'),
    #url(r'MyModel/(?P<pk>[0-9]+)/$', MyModeDetail.as_view(), name='MyModel-detail'),
    #url(r'MyModel/(?P<pk>[0-9]+)/edit/$', MyModelUpdate.as_view(), name='MyModel-update'),
    #url(r'MyModel/(?P<pk>[0-9]+)/delete/$', MyModelDelete.as_view(), name='MyModel-delete'),
    #url(r'MyModel/$', MyModelListView.as_view(), name='list'),
    #url(r'hd/', views.my_handler, name='my_handler'),
]
