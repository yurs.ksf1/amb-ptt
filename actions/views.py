from django.shortcuts import render
from actstream import action
from actstream.models import Action, actor_stream
# Create your views here.

def home(request):
	msj = "Hello actions." , request.user.username
	template = 'msj1.html'
	context = {'msj': msj,}
	return render(request, template, context)


def listAct(request):
    queryset = Action.objects.filter(verb=ProfileVerbs("see"))

    #queryset = Action.objects.filter(actor=request.user)
    #queryset = actor_stream(request.user)

    #user_instance = User.objects.all()[0]
    #Action.objects.mystream(user_instance, verb= ProfileVerbs("see"))
    # OR
    #user_instance.actor_actions.mystream(verb = ProfileVerbs("see"))
    #print(user_instance)
    msj = queryset
    template = 'msj1.html'
    context = {'msj': msj,}
    return render(request, template, context)



def listAct1(request):
    #queryset = Action.objects.filter(verb=ProfileVerbs("see"))
    queryset = Action.objects.filter(actor=request.user.pk)
    print(queryset)
    msj = queryset
    template = 'msj1.html'
    context = {'msj': msj,}
    return render(request, template, context)


from actstream.models import actor_stream

#actor_stream(request.user)
def MyAct(request):
    print(actor_stream(request.user))
    queryset = Action.objects.filter(verb=ProfileVerbs("edit"))
    msj = queryset
    template = 'msj1.html'
    context = {'msj': msj,}
    return render(request, template, context)


class ProfileVerbs():
    VERBS = {
        "edit": "Editó",
        "see": "Vió",
        "create": "Creó",
        "del": "Eliminó",
        "save": "Guardó",
    }
    def __init__(self, verb_key):
        self.verb_key = verb_key

    def __str__(self):
        return self.VERBS[self.verb_key]
