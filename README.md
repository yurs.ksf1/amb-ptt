# Readme content

## Synopsis

Este es el readme de configuracion para la aplicacion de Gestor de Expediente para el Área Metropolitana de Bucaramanga.

## Pre-Instalación
se presentan los requisitos que se deben tener antes de instalar
### Instalar Anaconda
descargar Anaconda
```
cd Downloads/
wget https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh
sh Anaconda3-5.0.1-Linux-x86_64.sh
```
* se inicia la instalacino, enter, yes.
* ubicacioni /opt/anaconda3
* path no

agregamos el path
```
export PATH=/opt/anaconda3/bin:$PATH
```
## Installation
ir al directorio donde estará el proyecto
```
cd /srv/
```

descargar el proyecto del repositorio
```
git clone https://gitlab.com/yurs.ksf1/amb-ptt.git
```

crear el entorno virtual con conda y activarlo
```
conda create --prefix=./venv python=3.5
source activate venv
```

instalar las dependencias del proyecto
```
conda install --prefix=./venv -c esri arcgis
pip install -r requirements.txt
```

migrar la base de datos, crear superusuario
```
python manage.py migrate
python manage.py createsuperuser
```

correr el servidor
```
python manage.py runserver 0.0.0.0:8000
```

## Contributors

* Yurley Sanchez
* Gustavo



## License

* UIS
* Geomatica - Grupo de Investigacion y Desarrollo
