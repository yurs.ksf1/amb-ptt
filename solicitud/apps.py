from django.apps import AppConfig


class SolicitudConfig(AppConfig):
    name = 'solicitud'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('PTT'))
 #       registry.register(User)
        from django.contrib.auth.models import User, Group, Permission
        registry.register(User)
        registry.register(Group)
        registry.register(Permission)
