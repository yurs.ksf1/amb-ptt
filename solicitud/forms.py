
from .models import PTT
from django import forms


class PTTForm(forms.ModelForm):
	class Meta:
		model = PTT
		fields = ('address', 'neighborhood', 'municipality', 'space', 'motive', 'height', 'description','keyword','poda','tala','traslado')
		layout = [
			("Text", "<h4 class=\"ui dividing header\">Direccion</h4>"),
			("Three Fields",
				("Field", "municipality"),
				("Field", "neighborhood"),
				("Field", "address"),
			),
			("Text", "<h4 class=\"ui dividing header\">Detalles del Los individuos</h4>"),
			("Three Fields",
			("Field", "space"),
			("Field", "motive"),
			("Field", "height"),
			),
			("Text", "<h4 class=\"ui dividing header\">Tratamieno a los individuos</h4>"),
			("Three Fields",
			("Field", "poda"),
			("Field", "tala"),
			("Field", "traslado"),
			),
			("Text", "<h4 class=\"ui dividing header\">Descripción</h4>"),
			("Two Fields",
			("Field", "description"),
			("Field", "keyword"),
			),
		]

	def __init__(self, *args, **kwargs):
		super(__class__, self).__init__(*args, **kwargs)

		placeholders = {
			"municipality": "municipality",
			"neighborhood": "neighborhood",
			"address": "address",
		}

		choicer = {
			"municipality": "search",
			"neighborhood": "search",
			"space": "search",
			"motive": "search",
			"height": "search",
		}

		for key, value in placeholders.items():
			self.fields[key].widget.attrs["placeholder"] = value

		for key, value  in choicer.items():
			self.fields[key].widget.attrs["_style"] = value
