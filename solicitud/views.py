from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView

from guardian.compat import reverse_lazy
from guardian.mixins import PermissionRequiredMixin, PermissionListMixin
from guardian.shortcuts import assign_perm
from solicitud.models import PTT, PTT2
from common.Yurs import getUnike, getRandomSome, getRandomSome1
from .forms import PTTForm
from myprofile.models import UserProfile
from django.shortcuts import render
from common.actions import CommonVerbs
from actstream import action
from common.choices import SOLICITUDES

from django.db.models import Q

def RequestListView(request):

    template = 'req_list.html'
    context = {
    }

    return render(request, template, context)


class CounterMixin(object):

    def get_context_data(self, **kwargs):
        context = super(CounterMixin, self).get_context_data(**kwargs)
        context['count'] = self.get_queryset().count()
        return context


class SearchMixin(object):

    def get_queryset(self):
        qset = super(SearchMixin, self).get_queryset()
        q = self.request.GET.get('search')
        if q:
            q = q.split(' ')
            for w in q:
                qset = qset.filter(
                    #Q(id__contains=w)|
                    Q(name__contains=w)|
                    Q(radicade__contains=w)|
                    Q(description__contains=w)
                    #Q(inscripcion_numero__contains=w) |
                    #Q(expedientepartida__partida__pii__contains=w) |
                    #Q(expedientepersona__persona__nombres__icontains=w) |
                    #Q(expedientepersona__persona__apellidos__icontains=w) |
                    #Q(expedientelugar__lugar__nombre__icontains=w)
                ).distinct()
        return qset


class SolicitudRecepcionMixin(object):

    def get_queryset(self):
        qset = super(SolicitudRecepcionMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='Reciente')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset

class SolicitudProcessMixin(object):

    def get_queryset(self):
        qset = super(SolicitudProcessMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='En Proceso')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset

class SolicitudHistoryMixin(object):

    def get_queryset(self):
        qset = super(SolicitudHistoryMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='Historicas')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset


class PTTRListView(CounterMixin, SearchMixin, SolicitudRecepcionMixin, ListView):
    template_name = 'solicitud/ptt_1Rlist.html'
    model = PTT
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PTTRListView, self).dispatch(*args, **kwargs)


class PTTPListView(CounterMixin, SearchMixin, SolicitudProcessMixin, ListView):
    template_name = 'solicitud/ptt_1Plist.html'
    model = PTT
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PTTPListView, self).dispatch(*args, **kwargs)


class PTTHListView(CounterMixin, SearchMixin, SolicitudHistoryMixin, ListView):
    template_name = 'solicitud/ptt_1Hlist.html'
    model = PTT
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PTTHListView, self).dispatch(*args, **kwargs)



class PTTListView(CounterMixin, SearchMixin, ListView):
    template_name = 'solicitud/ptt_list.html'
    paginate_by = 10
    model = PTT

    permission_required = ['view_ptt',]

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PTTListView, self).dispatch(*args, **kwargs)




from django.views.generic.edit import FormView
class PTTCreateView(PermissionRequiredMixin, FormView):
    permission_object = None
    permission_required = ['solicitud.add_ptt']

    template_name = 'solicitud/ptt_form.html'
    form_class = PTTForm
    success_url = '../1'



    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)
        self.object.radicade = getUnike()
        self.object.state = 'Reciente'
        self.object.petitioner = UserProfile.objects.get(user = self.request.user)
        self.object.typeRequest = 'Permiso de tala y poda'
        self.object.save()

        assign_perm('view_ptt', self.request.user, self.object)
        assign_perm('change_ptt', self.request.user, self.object)
        assign_perm('delete_ptt', self.request.user, self.object)

        action.send(self.request.user, verb=CommonVerbs("create"), description= "solicitud", action_object=self.object)
        return super(PTTCreateView, self).form_valid(form)

class PTTDetailView(PermissionRequiredMixin, DetailView):
    model = PTT
    permission_required = ['view_ptt']

    slug_url_kwarg = 'id'
    slug_field = 'id'
    #action.send(self.request.user, verb=CommonVerbs("see"), description= "solicitud", action_object=self.object)

class PTTUpdateView(PermissionRequiredMixin, UpdateView):
    model = PTT

    slug_url_kwarg = 'id'
    slug_field = 'id'
    permission_required = ['view_ptt', 'change_ptt']
    template_name = 'solicitud/ptt_form.html'
    form_class = PTTForm
    success_url = '../../1'

    def form_valid(self, form):
        action.send(self.request.user, verb=CommonVerbs("edit"), description= "solicitud", action_object=self.object)
        self.object = form.save()
        # en esta parte toca colocar lo de action stream

        return super(PTTUpdateView, self).form_valid(form)
    #permission_required = ['view_mymodel', 'change_mymodel']
    #fields = ('__all__')

class PTTDeleteView(PermissionRequiredMixin, DeleteView):
    model = PTT

    slug_url_kwarg = 'id'
    slug_field = 'id'
    success_url = reverse_lazy('solicitud:list')
    permission_required = ['view_ptt', 'delete_ptt']
    #action.send(self.request.user, verb=CommonVerbs("del"), description= "solicitud", action_object=self.object)


# from PTT2
class PTT2ListView(PermissionListMixin, ListView):
    model = PTT2
    #permission_required = ['view_ptt',]
    permission_required = ['view_ptt2',]

class PTT2CreateView(PermissionRequiredMixin, CreateView):
    model = PTT2
    permission_object = None
    permission_required = ['solicitud.add_ptt2']
    fields = (
        'municipality',
        'neighborhood',
        'address',
        'sample',
    )



    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save(commit=False)
        self.object.radicade = getUnike()
        self.object.petitioner = UserProfile.objects.get(user = self.request.user)
        self.object.typeRequest = getRandomSome(SOLICITUDES)
        self.object.save()

        assign_perm('view_ptt2', self.request.user, self.object)
        assign_perm('change_ptt2', self.request.user, self.object)
        assign_perm('delete_ptt2', self.request.user, self.object)

        #action.send(self.request.user, verb=CommonVerbs("create"), description= "solicitud", action_object=self.object)
        return super(PTT2CreateView, self).form_valid(form)

class PTT2DetailView(PermissionRequiredMixin, DetailView):
    model = PTT2
    permission_required = ['view_ptt2']

    slug_url_kwarg = 'id'
    slug_field = 'id'
    #action.send(self.request.user, verb=CommonVerbs("see"), description= "solicitud", action_object=self.object)

class PTT2UpdateView(PermissionRequiredMixin, UpdateView):
    model = PTT2

    slug_url_kwarg = 'id'
    slug_field = 'id'
    permission_required = ['view_ptt2', 'change_ptt2']
    fields = (
    'municipality',
    'neighborhood',
    'address',
    'sample',
    )
    def form_valid(self, form):
        #action.send(self.request.user, verb=CommonVerbs("edit"), description= "solicitud", action_object=self.object)
        self.object = form.save()
        # en esta parte toca colocar lo de action stream

        return super(PTT2UpdateView, self).form_valid(form)
    #permission_required = ['view_mymodel', 'change_mymodel']
    #fields = ('__all__')

class PTT2DeleteView(PermissionRequiredMixin, DeleteView):
    model = PTT2

    slug_url_kwarg = 'id'
    slug_field = 'id'
    success_url = reverse_lazy('solicitud:list')
    permission_required = ['view_ptt2', 'delete_ptt2']
    #action.send(self.request.user, verb=CommonVerbs("del"), description= "solicitud", action_object=self.object)
