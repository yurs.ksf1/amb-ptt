# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from . import views


app_name = 'solicitud'
urlpatterns = [
    url(r'^$',                                  views.RequestListView,                name="list"),

    # vista de estados de la solicitudud.
    url(r'^1/$',                                views.PTTListView.as_view(),           name="list2"),
    url(r'^1/reception$',                       views.PTTRListView.as_view(),           name="list1R"),
    url(r'^1/process$',                         views.PTTPListView.as_view(),           name="list1P"),
    url(r'^1/history$',                         views.PTTHListView.as_view(),           name="list1H"),


    # vistas por cada objeto soliitud desde creacion, edicion, vista, y eliminacion (CRUD)
    url(r'^1/~create$',                         views.PTTCreateView.as_view(),         name="create"),
    url(r'^1/(?P<id>[0-9a-z-]+)/$',             views.PTTDetailView.as_view(),         name="details"),
    url(r'^1/(?P<id>[0-9a-z-]+)/~update$',      views.PTTUpdateView.as_view(),         name="update"),
    url(r'^1/(?P<id>[0-9a-z-]+)/~delete$',      views.PTTDeleteView.as_view(),         name="delete"),

    url(r'^2/~create$',                         views.PTT2CreateView.as_view(),         name="ptt2-create"),
    url(r'^2/(?P<id>[0-9a-z-]+)/$',             views.PTT2DetailView.as_view(),         name="ptt2-details"),
    url(r'^2/(?P<id>[0-9a-z-]+)/~update$',      views.PTT2UpdateView.as_view(),         name="ptt2-update"),
    url(r'^2/(?P<id>[0-9a-z-]+)/~delete$',      views.PTT2DeleteView.as_view(),         name="ptt2-delete"),
]
