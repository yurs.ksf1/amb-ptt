from django.contrib import admin
from django.contrib.admin import DateFieldListFilter

from .models import PTT, PTT2

from guardian.admin import GuardedModelAdmin

# estos inlines se usan en expedientes.
class PTTInline(admin.StackedInline):
    model = PTT
    extra = 0

class PTT2Inline(admin.StackedInline):
    model = PTT
    extra = 0

class PTTAdmin(GuardedModelAdmin):
    list_display = ('radicade','idExpedient', 'state', 'space', 'createDate','answerDate')
    list_filter = ('municipality', 'state', 'typeRequest', 'idExpedient', 'space', 'motive', 'height',
        ('modified', DateFieldListFilter),
        ('createDate', DateFieldListFilter),
        ('answerDate', DateFieldListFilter),
    )
    search_fields = ('radicade', 'petitioner__firsrtName', 'idExpedient__id', 'keyword', 'description',)

admin.site.register(PTT, PTTAdmin)


class PTT2Admin(GuardedModelAdmin):
    list_display = ('radicade','idExpedient', 'state','createDate','answerDate')
    list_filter = ('municipality', 'state', 'typeRequest', 'idExpedient', 'sample',
        ('modified', DateFieldListFilter),
        ('createDate', DateFieldListFilter),
        ('answerDate', DateFieldListFilter),
    )
    search_fields = ('radicade', 'petitioner__firsrtName', 'idExpedient__id', 'municipality')

admin.site.register(PTT2, PTT2Admin)
