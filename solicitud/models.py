from __future__ import unicode_literals
import uuid

from django.db import models
from guardian.compat import reverse

from django_extensions.db.models import TimeStampedModel

from guardian.models import GroupObjectPermissionBase, UserObjectPermissionBase

from expedientes.models import Expedient
from django.contrib.auth.models import User
from myprofile.models import UserProfile
from common.choices import STATE, COORDINACIONES, MUNICIPIOS, BARRIOS, SOLICITUDES, BANCOS, ADJUNTOS, TERRITORIOS, JUSTIFICACIONES, ALTURAS
from datetime import timezone, datetime, timedelta, date
from common.Yurs import getUnike
from spatialMod.loadRequest import AddSpatialRequest, DelSpatialRequest

from django.db.models.signals import post_save, pre_delete
"""
class Request(models.Model):
    title = models.CharField('title', max_length=64)
    slug = models.SlugField(max_length=64)
    content = models.TextField('content')
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        permissions = (
            ('view_request', 'Can view request'),
        )
        get_latest_by = 'created_at'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('solicitud:details', kwargs={'slug': self.slug})
"""

class Request(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    radicade = models.CharField("Numero de Radicado", max_length=200,  blank=True, null=True, unique=True)  # Agregar en el modelo!!! Importante!!!!

    idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    petitioner = models.ForeignKey( UserProfile, blank=True, null=True, on_delete=models.CASCADE)
    typeRequest = models.CharField("Tipo de solicitud", max_length=100,choices=SOLICITUDES)  # esto puede ser de otro modelo, y que este modelo sea abstracto y se agregen cosas o algo asi... jaja
    address = models.CharField("Dirección", max_length=100, default=' ')
    neighborhood = models.CharField("Barrio", max_length=100, choices=BARRIOS, default=' ')
    municipality = models.CharField("Municipio", max_length=100,choices=MUNICIPIOS, default=' ')

    description =  models.TextField("Descripción de la solicitud")
    keyword =  models.CharField("Palabras Clave", blank=True, null=True, max_length=100, )
    createDate = models.DateField("Fecha de creacion", default=date.today)  # Agregar!
    answerDate = models.DateField("Fecha estimada de Respuesta", default=datetime.now()+timedelta(days=18))
    approved = models.BooleanField(default=False)
    resolved = models.BooleanField(default=False)
    state = models.CharField("Estado", max_length=100,choices=STATE, default=' ')

    name = models.CharField("nombre", max_length=200, blank=True, null=True, )
    description = models.TextField("descripcion", max_length=200, blank=True, null=True, )

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('solicitud:details', kwargs={'id': self.id})



        super(Expedient, self).save()

    class Meta:
        permissions = (
            ('view_request', 'Can view request'),
        )
        verbose_name = ("Solicitud")
        verbose_name_plural = ("Solicitudes")
        abstract = True


class PTT(Request):
    space = models.CharField("Territorio", max_length=100,choices=TERRITORIOS)
    motive = models.CharField("Motivo", max_length=100,choices=JUSTIFICACIONES)
    height = models.CharField("Altura", max_length=100,choices=ALTURAS)
    poda = models.PositiveSmallIntegerField("Numero de Podas",default=0)
    tala = models.PositiveSmallIntegerField("Numero de Talas",default=0)
    traslado = models.PositiveSmallIntegerField("Numero de Traslados",default=0)
    #photo = models.FileField("Fotorgrafia ", null=True, blank=True, upload_to='uploads/')

    class Meta:
        permissions = (
            ('view_ptt', 'Can view ptt'),
        )
        get_latest_by = 'createDate'
        verbose_name = ("PTT")
        verbose_name_plural = ("PTT")


    def __unicode__(self):
        return self.radicade

    def get_absolute_url(self):
        return reverse('solicitud:details', kwargs={'id': self.id})

    def save(self):
        if(not self.idExpedient):
            # esto es porque tiene expediente pero no se ha respondido
            self.state = 'Reciente'
        if(self.idExpedient and not self.resolved):
            # esto es porque tiene expediente pero no se ha respondido
            self.state = 'En Proceso'
            # pregunto si no tiene radicado y se lo agrego al estilo unix
            #if(not self.radicade):
            #    self.object.radicade = getUnike()
        if(self.resolved):
            # esto es porque ya respondieron, pero aun no esta cerrado -- osea en seguimeito (algo esta mal con respecto a lo que le dije a lucho pero luego lo arreglo)
            # falta lo del seguimeito que deveria quedar en proceso, y leugo de eso pasa a cerrado historico..
            self.state = 'Historicas'


        if(not self.radicade):
            self.radicade = getUnike()

        if(not self.name):
            if(not self.radicade):
                # esto es porque tiene expediente pero no se ha respondido
                self.name = self.radicade + " - " + self.typeRequest


        super(PTT, self).save()


def sentSpatialRequest(sender, **kwargs):
    request = kwargs["instance"]
    if kwargs["created"]:
        #print(sender)
        AddSpatialRequest(request)

def delSpatialRequest(sender, **kwargs):
    request = kwargs["instance"]
    #if kwargs["created"]:
        #print(sender)
    DelSpatialRequest(request)

post_save.connect(sentSpatialRequest, sender=PTT)
pre_delete.connect(delSpatialRequest, sender=PTT)

class PTT2(Request):
    sample = models.CharField("Sample", max_length=100, default=' ')

    class Meta:
        permissions = (
            ('view_ptt2', 'Can view ptt2'),
        )
        get_latest_by = 'createDate'
        verbose_name = ("PTT2")
        verbose_name_plural = ("PTT2")


    def __unicode__(self):
        return self.radicade

    def get_absolute_url(self):
        return reverse('solicitud:ptt2-details', kwargs={'id': self.id})


class PTTUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(PTT, on_delete=models.CASCADE)


class PTTGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(PTT, on_delete=models.CASCADE)

class PTT2UserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(PTT2, on_delete=models.CASCADE)


class PTT2GroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(PTT2, on_delete=models.CASCADE)
