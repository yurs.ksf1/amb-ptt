from django.conf.urls import url

from . import views


app_name = 'spatial'

urlpatterns = [
    url(r'^see/$',               views.see,           name="see"),
    url(r'^edit/$',              views.edit,           name="edit"),
 ]
