
from arcgis.gis import GIS
from arcgis.geocoding import geocode
from datetime import datetime

portal_url = "http://gorrion.uis.edu.co/arcgis/"
username = 'automata.apps'
password = 'Auto.APS-Go17'

gis = GIS(portal_url, username, password)

ports_item = gis.content.get('4ca58048751e46b48aef288efb2dbca1')

ports_layers= ports_item.layers

ports_fset = ports_layers[19].query() #an empty query string will return all
ports_flayer = ports_layers[19]

ports_features = ports_fset.features

def AddSpatialRequest(request):
    search = request.address + ", " +request.neighborhood+ ", " +request.municipality+ ", " +"Santander" +  ", " +"Colombia"
    solicitud = geocode(search)[0]
    date = int((datetime.timestamp(datetime.now())*1000))
    la_dict = {"attributes":
    {
        'ACTIVIDADCIIU': 'NR',
        'BARRIOPOT': 0,
        'NOMBRE': request.petitioner.firsrtName + " " + request.petitioner.lastName,
        'BARRIO': request.neighborhood,
        'DESCRIPCION': request.description,
        'DIRECCION': request.address,
        'CORTE': request.tala,
        'PODA': request.poda,
        'TRASLADO':request.traslado,
        'EXPEDIENTE': request.radicade,
        'TIPOVISITA':1,
        'OBSERVACION': None,
        'IDVISITAFLORA': 6565,
        'CAR': 2,
        'CODIGO': None,
        'COMISIONADO':  None,
        'COMPENSACION': None,
        'COORDLATITUD': None,
        'COORDLONGITUD': None,
        'COORDX': None,
        'COORDY': None,
        'CORREGIMIENTO': None,
        'DEPARTAMENTO': 68,
        'EJECUCION': 1,
        'ESTADO':1,
        'FECHACREACION': date,
        'MUNICIPIO': 307,
        'OBJECTID': None,
        'PROYECTO': None,
        'REGISTRO': None,
        'SECCIONCIIU': 'NR',
        'VEREDA': None,
        'created_date': date,
        'created_user': 'automata.apps',
        'last_edited_date': date,
        'last_edited_user': 'automata.apps'},
        "geometry":
        solicitud['location']}

    add_result = ports_flayer.edit_features(adds = [la_dict])

def DelSpatialRequest(request):
    ports_fset = ports_layers[19].query() #an empty query string will return all
    ports_features = ports_fset.features
    for f in ports_features:
        if f.attributes['EXPEDIENTE']==request.radicade:
            Redlands_feature = f
            Redlands_objid = Redlands_feature.get_value('OBJECTID')
            delete_result = ports_flayer.edit_features(deletes=str(Redlands_objid))
