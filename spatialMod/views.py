from django.shortcuts import render
from django.http import HttpResponseRedirect
import requests

def generateToken(username, password, serverUrl, portalUrl):
    payload = dict(request='getToken',
               username=username,
               password=password,
               ip = "",
               referer = serverUrl,
               expiration='60',
               f='json')
    url_requests= portalUrl + "/sharing/generateToken"
    #print(url_requests)
    r = requests.post(url_requests, data=payload)
    return r


def getToken(url):
	username = 'automata.apps'
	password = 'Auto.APS-Go17'
	serverUrl = url
	portalUrl = 'https://gorrion.uis.edu.co/arcgis'

	r = generateToken(username, password, serverUrl, portalUrl)

	data = r.json()
	token = data['token']
	return token


def see(request):
	url = "http://gorrion.uis.edu.co/arcgis/apps/webappviewer/index.html?id=c509af9eb3da4fb0bd154317e76f5952"
	#token = "&token=bUFxggVp0EvNwC5rN23l4C8HWm_f0j7aulGkZZmeOy6pZrT4UhhATqO8mjJPrQQpVsHjRn4Ai3iB1LmgL8RyeGMeu3XvbYHG3CAlSBAuz8aNRcOK5rPhyIIaoPl-EQ7lhE-hs9plYSrCpTVKcthWhQ890S3DgUG2E7-0C4bQb6bAfuSdu_ooDh5adCCcbZ75VKM03qRJv2DlWD5KpicXM_N49pbrBU6TQhBf-Rm-g1c"
	#token = "&token=QdH-yAJwM-T92XRGCSUBM0K8BT4GdkueLC-FXr1qeQ13_Qb8I3vihMrfBhtiiBOfaqCjAWotgx6OZR0d1Rw8WIyZ59-xZcJhb35wWjCHP7XBVFyA0iMpOeV87dvdNIqWLUHD0fuOMzSyraGes2sVuj5i339Pg0SN6_svqLYuDJr51rfWYyLWmeykPm7-Eie_-0T7JDh2vdnDR4Rs8aX8-C3HGiGGbEmuyNv8bF84-46MTsJAeU3qHjvnaFpmj34W"
	token =  getToken(url)
	token = "&token=" + token
	my_url = url + token
	return HttpResponseRedirect(my_url)


def edit(request):
	url = "http://gorrion.uis.edu.co/arcgis/apps/webappviewer/index.html?id=c509af9eb3da4fb0bd154317e76f5952"
	#token = "&token=bUFxggVp0EvNwC5rN23l4C8HWm_f0j7aulGkZZmeOy6pZrT4UhhATqO8mjJPrQQpVsHjRn4Ai3iB1LmgL8RyeGMeu3XvbYHG3CAlSBAuz8aNRcOK5rPhyIIaoPl-EQ7lhE-hs9plYSrCpTVKcthWhQ890S3DgUG2E7-0C4bQb6bAfuSdu_ooDh5adCCcbZ75VKM03qRJv2DlWD5KpicXM_N49pbrBU6TQhBf-Rm-g1c"
	token = "&token=QdH-yAJwM-T92XRGCSUBM0K8BT4GdkueLC-FXr1qeQ13_Qb8I3vihMrfBhtiiBOfaqCjAWotgx6OZR0d1Rw8WIyZ59-xZcJhb35wWjCHP7XBVFyA0iMpOeV87dvdNIqWLUHD0fuOMzSyraGes2sVuj5i339Pg0SN6_svqLYuDJr51rfWYyLWmeykPm7-Eie_-0T7JDh2vdnDR4Rs8aX8-C3HGiGGbEmuyNv8bF84-46MTsJAeU3qHjvnaFpmj34W"

	my_url = url + token
	return HttpResponseRedirect(my_url)
