# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url
from . import views

app_name = 'common'

urlpatterns = [
    # index
    url(r'^$',        views.index,     name='index'),
    # None
    url(r'^none/',    views.None404,        name='none'),
    # About
    url(r'^about/$', views.About.as_view(), name='about'),

    # About
    url(r'^config/$', views.config, name='config'),
]
