from django.shortcuts import render
from django.views.generic import TemplateView
from .config_1 import initConfiguraciones

def config(request):
    initConfiguraciones()
    msj = "configurado"
    template = 'index.html'
    context = {'msj': msj,}
    return render(request, template, context)

def index(request):
    msj = "Hello index."
    template = 'index.html'
    context = {'msj': msj,}
    return render(request, template, context)

def None404(request):
    msj = "We Are Working."
    template = 'home/404.html'
    context = {'msj': msj,}
    return render(request, template, context)

class About(TemplateView):
    template_name = 'home/about.html'
