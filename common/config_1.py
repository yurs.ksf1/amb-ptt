
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from solicitud.models import PTT
from django.contrib.auth.models import User
from myprofile.models import UserProfile


def initConfiguraciones():
    if(not User.objects.filter(username = 'adminSol')):
        print("configurando")

        usuario = User.objects.create_user(username='adminSol', password='yurleysanchez', is_staff='True', is_superuser = 'True')
        usuario = User.objects.create_user(username='user',     password='yurleysanchez')

        usr = User.objects.get(username = 'adminSol')
        usr.first_name = "Admin"
        usr.last_name =  "Solicitudes"
        usr.email =  "Admin.Solicitante.Demo@mail.com"
        usr.save()

        usr = User.objects.get(username = 'user')
        usr.first_name = "Solicitante"
        usr.last_name =  "Demo"
        usr.email =  "Solicitante.Demo@mail.com"
        usr.save()

        usr_p = UserProfile.objects.get(user = usr)
        usr_p.numDoc = "10101010"
        usr_p.firsrtName = "Solicitante User"
        usr_p.lastName = "Demo Django"
        usr_p.gender = "Mujer"
        usr_p.municipality = "Bucaramanga"
        usr_p.address = "Cra 27 N 9 - 25"
        usr_p.phone = "6344000"
        usr_p.save()


        # creamos el permiso
        mymodel = PTT
        content_type = ContentType.objects.get_for_model(mymodel)
        permission = Permission.objects.filter(content_type=content_type, codename__startswith='add_')[0]

        # creamos el Grupo
        g_sol = Group()
        g_sol.name = "solicitantes"
        g_sol.save()
        # agregamos el permiso al grupo
        g_sol.permissions.add(permission)

        # creamos el permiso
        mymodel = PTT
        content_type = ContentType.objects.get_for_model(mymodel)
        permission = Permission.objects.filter(content_type=content_type, codename__startswith='change_')[0]

        # creamos el Grupo
        g_adm_sol = Group()
        g_adm_sol.name = "admin Solicitudes"
        g_adm_sol.save()
        # agregamos el permiso al grupo
        g_adm_sol.permissions.add(permission)

        #add user to group
        usr =User.objects.get(username = 'user')
        usr.groups.add(g_sol)

        usr =User.objects.get(username = 'adminSol')
        usr.groups.add(g_adm_sol)




        print("fin configuracion")
    else:
        print("ya esta configurado -- creo")

    """
    # creamos el permiso
    mymodel = PTT
    content_type = ContentType.objects.get_for_model(mymodel)
    permission = Permission.objects.filter(content_type=content_type, codename__startswith='add_')[0]

    # creamos el Grupo
    g_sol = Group()
    g_sol.name = "solicitantes"
    g_sol.save()
    # agregamos el permiso al grupo
    g_sol.permissions.add(permission)

    # creamos el permiso
    mymodel = PTT
    content_type = ContentType.objects.get_for_model(mymodel)
    permission = Permission.objects.filter(content_type=content_type, codename__startswith='change_')[0]

    # creamos el Grupo
    g_adm_sol = Group()
    g_adm_sol.name = "admin Solicitudes"
    g_adm_sol.save()
    # agregamos el permiso al grupo
    g_adm_sol.permissions.add(permission)
    """

#initConfiguraciones()
