import uuid

from django.db import models
from django.contrib.auth.models import User
#from expedientes.models import Evidence

from django_extensions.db.models import TimeStampedModel

from django.utils import timezone



def CapitalizePhrase(string):
    phrase = ''
    for word in string.split():
        phrase = u'%s %s%s' % (phrase, word[0].upper(), word[1:].lower())
    return phrase.strip()



class Timestampable(models.Model):
    createDate = models.DateTimeField(auto_now_add=True)
    modifyDate = models.DateTimeField(auto_now=True)

    #class Meta:
    #    abstract = True
"""
class TimeStampedModel(models.Model):
    # TimeStampedModel
    # An abstract base class model that provides self-managed "created" and
    # "modified" fields.

    created = CreationDateTimeField(_('created'))
    modified = ModificationDateTimeField(_('modified'))

    def save(self, **kwargs):
        self.update_modified = kwargs.pop('update_modified', getattr(self, 'update_modified', True))
        super(TimeStampedModel, self).save(**kwargs)

    class Meta:
        get_latest_by = 'modified'
        ordering = ('-modified', '-created',)
        abstract = True
"""


class MyExampleT(TimeStampedModel):
    text = models.TextField("Descipcion", blank=True, null=True, help_text='An optional text to show.')



class Deadline(models.Model):
    #objects = DeadlineManager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    #idEvidence = models.ForeignKey(Evidence, blank=True, null=True, on_delete=models.CASCADE)
    deadline = models.DateTimeField("Fecha limite", help_text='The time of the deadline.')
    text = models.TextField("Descipcion", blank=True, null=True, help_text='An optional text to show.')
    isDone = models.BooleanField(default=False)

    class Meta:
        #app_label = 'core'
        verbose_name = 'Deadline'
        verbose_name_plural = 'Deadlines'
        ordering = ['-deadline']
        #abstract = True

    def __unicode__(self):
        return unicode(self.deadline)

    def __repr__(self):
        return 'Deadline(id={id}, deadline={deadline})'.format(**self.__dict__)

    def can_delete(self, user_obj):
        if self.id is None:
            return False
        if user_obj.is_superuser:
            return True
        else:
            return False

    def is_in_the_future(self):
        """
        Return ``True`` if this deadline is in the future.
        """
        return self.deadline > timezone.now()

    is_in_the_future.boolean = True

    def is_nearly(self, d=2):
        """
        Return ``True`` if this deadline is in the future and is nearly about d, default is 2 days
        """
        return (self.deadline < timezone.now() + timezone.timedelta(d) ) and (self.deadline > timezone.now())

    is_nearly.boolean = True


    def is_in_the_past(self):
        """
        Return ``True`` if this deadline is in the past.
        """
        return self.deadline < timezone.now()

    def has_text(self):
        """
        Checks that the text is not ``None`` or an empty string.
        """
        if self.text is None:
            return False
        else:
            return self.text.strip() != ''
