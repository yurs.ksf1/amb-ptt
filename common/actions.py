class CommonVerbs():
    VERBS = {
        "edit": "Editó",
        "see": "Vió",
        "create": "Creó",
        "del": "Eliminó",
        "save": "Guardó",
    }
    def __init__(self, verb_key):
        self.verb_key = verb_key

    def __str__(self):
        return self.VERBS[self.verb_key]
