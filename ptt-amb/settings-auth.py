from __future__ import absolute_import, unicode_literals

from .base import *  # NOQA

DEBUG = True
import ldap, logging
from django_auth_ldap.config import LDAPSearch, GroupOfNamesType

logger = logging.getLogger('django_auth_ldap')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

# Baseline configuration.
#AUTH_LDAP_SERVER_URI = "ldap://192.168.86.32"
AUTH_LDAP_SERVER_URI = "ldap://10.1.90.10"
AUTH_LDAP_BIND_DN = "CN=Automata Auth,CN=pseudo,CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co"
AUTH_LDAP_BIND_PASSWORD = "narf-AfhoHyifUjl"
#AUTH_LDAP_USER_SEARCH = LDAPSearch("CN=UsuariosTest,DC=kiwi,DC=uis,DC=edu,DC=co",ldap.SCOPE_SUBTREE, "(samAccountName=%(user)s)")
#AUTH_LDAP_USER_SEARCH = LDAPSearch("CN=tmp,CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co",ldap.SCOPE_SUBTREE, "(samAccountName=%(user)s)")
AUTH_LDAP_USER_SEARCH = LDAPSearch("CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co",ldap.SCOPE_SUBTREE, "(samAccountName=%(user)s)")

AUTHENTICATION_BACKENDS += (
    'django_auth_ldap.backend.LDAPBackend',
)

"""

# or perhaps:
# AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,ou=users,dc=example,dc=com"
"""
# Set up the basic group parameters.
AUTH_LDAP_GROUP_SEARCH = LDAPSearch("DC=kiwi,DC=uis,DC=edu,DC=co",
	ldap.SCOPE_SUBTREE,
)
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType()

"""
# Simple group restrictions
AUTH_LDAP_REQUIRE_GROUP = "cn=enabled,ou=django,ou=groups,DC=kiwi,DC=uis,DC=edu,DC=co"
AUTH_LDAP_DENY_GROUP = "cn=disabled,ou=django,ou=groups,DC=kiwi,DC=uis,DC=edu,DC=co"
"""

# Populate the Django user from the LDAP directory.
AUTH_LDAP_USER_ATTR_MAP = {
	"first_name": "givenName",
	"last_name": "sn",
	"email": "mail",
}
"""
AUTH_LDAP_PROFILE_ATTR_MAP = {
	"employee_number": "employeeNumber"
}
"""

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
	#"is_active": "cn=active,ou=django,ou=groups,dc=example,dc=com",
	"is_staff": "CN=amb_fase2,CN=Grupos,DC=kiwi,DC=uis,DC=edu,DC=co",
	"is_superuser": "CN=Administradores,CN=Builtin,DC=kiwi,DC=uis,DC=edu,DC=co"
}
"""
AUTH_LDAP_PROFILE_FLAGS_BY_GROUP = {
	"is_awesome": "cn=awesome,ou=django,ou=groups,dc=example,dc=com",
}
"""
# This is the default, but I like to be explicit.
AUTH_LDAP_ALWAYS_UPDATE_USER = True

# Use LDAP group membership to calculate group permissions.
AUTH_LDAP_FIND_GROUP_PERMS = True

# Cache group memberships for an hour to minimize LDAP traffic
AUTH_LDAP_CACHE_GROUPS = True
AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600


# Keep ModelBackend around for per-user permissions and maybe a local
# superuser.
""""
-- Info que me paso gustavo
server: 192.168.86.32
dominio: Kiwi - kiwi.uis.edu.co
user: automata.auth
pass: narf-AfhoHyifUjl
"""
"""
2.0
DC=kiwi,DC=uis,DC=edu,DC=co
CN=Automata Auth,CN=pseudo,CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co
CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co
sAMAccountName
-----

3.0
CN=Configuration,DC=kiwi,DC=uis,DC=edu,DC=co
CN=Schema,CN=Configuration,DC=kiwi,DC=uis,DC=edu,DC=co
DC=DomainDnsZones,DC=kiwi,DC=uis,DC=edu,DC=co
DC=ForestDnsZones,DC=kiwi,DC=uis,DC=edu,DC=co
DC=kiwi,DC=uis,DC=edu,DC=co


CN=Automata Auth,CN=pseudo,CN=Usuarios,DC=kiwi,DC=uis,DC=edu,DC=co

4.0  -- Nuevos usuarios porque cambie el CN ahora es UsuariosTest

	1 - Gustavo Django
	username: gustavo.django
	Pass: 1245.Directory

	2 - Yurs Python
	username: yurs.python
	Pass: 4512.Directory

"""
