"""ptt-amb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views
#from filebrowser.sites import site
admin.autodiscover()
#import xadmin
#xadmin.autodiscover()

urlpatterns = [

    url(r'^',                   include('common.urls',    namespace='com')),
    url(r'^admin/',             admin.site.urls),
    url(r'^activity/',          include('actstream.urls')),
    url(r'^auth/',              include('django.contrib.auth.urls')),
    url(r'^accounts/',          include('registration.backends.default.urls')),
    url(r'^accounts/profile/',  include('myprofile.urls')),
    url(r'^actions/',           include('actions.urls')),

    #url(r'xadmin/',            include(xadmin.site.urls)),
    #url(r'^myapp/',            include('myapp.urls')),
    #url(r'^test/',             include('test_app.urls')),
    #url(r'^article/',          include('articles.urls',        namespace='articles')),
    #url(r'^org/',              include('organizacion.urls',   namespace='org')),
    url(r'^exp/',               include('expedientes.urls',    namespace='exp')),
    url(r'^sol/',               include('solicitud.urls',    namespace='sol')),
    url(r'^spatial/',           include('spatialMod.urls',    namespace='spa')),
    #url(r'^common/', include('common.urls', namespace='common')),

]

from django.conf.urls import (
handler400, handler403, handler404, handler500
)

#handler400 = 'my_app.views.bad_request'
#handler403 = 'my_app.views.permission_denied'
handler404 = 'common.views.None404'
#handler500 = 'my_app.views.server_error'
