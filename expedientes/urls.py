# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url
from . import views



app_name = 'expediente'

urlpatterns = [
    #url(r'^$',                            views.home, name='home'),

    # Inicio
    url(r'^$', views.Home.as_view(), name='home'),
    # Expedientes
    url(r'^expedient/$', views.ExpedienteList.as_view(), name="expedientes13"),
    url(r'^expedient/$', views.ExpedienteList.as_view(), name="expedientes"),

    url(r'^expedient/reception$',  views.ExpedientRList.as_view(), name="listER"),
    url(r'^expedient/process$',    views.ExpedientPList.as_view(), name="listEP"),
    url(r'^expedient/history$',    views.ExpedientHList.as_view(), name="listEH"),


    # Details views
    # Expediente
    url(r'^expedient/(?P<id>[0-9a-z-]+)/$', views.ExpedienteDetail.as_view(),  name="expediente"),

    # evidences
    url(r'^evidence/attached/(?P<id>[0-9a-z-]+)/$', views.EvidenceAttachedDetail.as_view(),  name="attached"),
    url(r'^evidence/collection/(?P<id>[0-9a-z-]+)/$', views.EvidenceCollectionDetail.as_view(),  name="collection"),
]
