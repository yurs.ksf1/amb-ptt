from django.shortcuts import render_to_response,get_object_or_404, render
from django.template.context import RequestContext
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from common.Yurs import getUnike, getRandomSome, getRandomSome1

from django import forms
from django.db.models import Q

from .models import Expedient
from .models import *
from solicitud.models import PTT

from django.views.generic import TemplateView, ListView, DetailView


class CounterMixin(object):

    def get_context_data(self, **kwargs):
        context = super(CounterMixin, self).get_context_data(**kwargs)
        context['count'] = self.get_queryset().count()
        return context


class SearchMixin(object):

    def get_queryset(self):
        qset = super(SearchMixin, self).get_queryset()
        q = self.request.GET.get('search')
        if q:
            q = q.split(' ')
            for w in q:
                qset = qset.filter(
                    Q(id__contains=w) |
                    Q(name__contains=w)|
                    Q(description__contains=w)
                    #Q(inscripcion_numero__contains=w) |
                    #Q(expedientepartida__partida__pii__contains=w) |
                    #Q(expedientepersona__persona__nombres__icontains=w) |
                    #Q(expedientepersona__persona__apellidos__icontains=w) |
                    #Q(expedientelugar__lugar__nombre__icontains=w)
                ).distinct()
        return qset

class ExpedientRecepcionMixin(object):

    def get_queryset(self):
        qset = super(ExpedientRecepcionMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='Reciente')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset

class ExpedientProcessMixin(object):

    def get_queryset(self):
        qset = super(ExpedientProcessMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='En Proceso')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset

class ExpedientHistoryMixin(object):

    def get_queryset(self):
        qset = super(ExpedientHistoryMixin, self).get_queryset()
        qset = qset.filter(
            Q(state='Historicas')
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )
        return qset


class ExpedientRList(CounterMixin, SearchMixin, ExpedientRecepcionMixin, ListView):
    template_name = 'lists/expediente_list.html'
    model = Expedient
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ExpedientRList, self).dispatch(*args, **kwargs)


class ExpedientPList(CounterMixin, SearchMixin, ExpedientProcessMixin, ListView):
    template_name = 'lists/expediente_list.html'
    model = Expedient
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ExpedientPList, self).dispatch(*args, **kwargs)


class ExpedientHList(CounterMixin, SearchMixin, ExpedientHistoryMixin, ListView):
    template_name = 'lists/expediente_list.html'
    model = Expedient
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ExpedientHList, self).dispatch(*args, **kwargs)


class ExpedienteAbiertoMixin(object):

    def get_queryset(self):
        qset = super(ExpedienteAbiertoMixin, self).get_queryset()
        qset = qset.filter(
            #Q(modifyDate__gt=datetime.date(2017, 10, 5))
            #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
            #Q(1=1)
            #Q(inscripcion_numero__isnull=True) &
            #Q(cancelado=False) &
            #Q(sin_inscripcion=False)
        )[:10]
        return qset


class Home(CounterMixin, SearchMixin, ExpedienteAbiertoMixin, ListView):
    template_name = 'home/index.html'
    model = Expedient

    def get_context_data(self, *args, **kwargs):
        context = super(Home, self).get_context_data(*args, **kwargs)
        # last 5 inscriptos
        context['insc_list'] = \
            Expedient.objects.filter(
                #Q(modifyDate__gt=datetime.date(2017, 5, 10))
                #Q(id='336f8ecd-9eaa-4644-80f6-141300c07eb9')
                #Q(modified__isnull=False) &
                #Q(inscripcion_fecha__isnull=False)
            ).order_by('-modified')[:5]
        return context

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Home, self).dispatch(*args, **kwargs)


class ExpedienteList(CounterMixin, SearchMixin, ListView):
    template_name = 'lists/expediente_list.html'
    model = Expedient
    paginate_by = 10

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class
        property value.
        """
        return self.request.GET.get('paginate_by', self.paginate_by)

    #@method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ExpedienteList, self).dispatch(*args, **kwargs)

class ExpedienteDetail(DetailView):
    template_name = 'lists/expediente_detail.html'
    model = Expedient
    slug_url_kwarg = 'id'
    slug_field = 'id'


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ExpedienteDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ExpedienteDetail, self).get_context_data(**kwargs)
        context['attached_list'] = Attached.objects.filter(idExpedient=self.object.id)
        context['collection_list'] = Collection.objects.filter(idExpedient=self.object.id)
        context['solicitud_list'] = PTT.objects.filter(idExpedient=self.object.id)

        return context

class EvidenceAttachedDetail(DetailView):
    template_name = 'evidence/attached.html'
    model = Attached
    slug_url_kwarg = 'id'
    slug_field = 'id'


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EvidenceAttachedDetail, self).dispatch(*args, **kwargs)


class EvidenceCollectionDetail(DetailView):
    template_name = 'evidence/collection.html'
    model = Collection
    slug_url_kwarg = 'id'
    slug_field = 'id'


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EvidenceCollectionDetail, self).dispatch(*args, **kwargs)
