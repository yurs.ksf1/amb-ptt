import uuid
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from common.choices import STATE, COORDINACIONES, MUNICIPIOS, SOLICITUDES, BANCOS, ADJUNTOS, TERRITORIOS, JUSTIFICACIONES, ALTURAS
from django.urls import reverse
from myprofile.models import UserProfile
from datetime import  datetime, timedelta, date
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel
from common.models import Deadline, CapitalizePhrase
from common.Yurs import getDateDay
#from filebrowser.fields import FileBrowseField

#from filer.fields.image import FilerFileField

class Expedient(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("nombre", max_length=200, blank=True, null=True, )
    description = models.TextField("descripcion", max_length=200, blank=True, null=True, )
    state = models.CharField("Estado", max_length=100,choices=STATE, default=' ')

    def get_absolute_url(self):
        return reverse('expediente:expediente', kwargs={'id': self.id})
        #return reverse('expedientes', kwargs={'id': str(self.id)})
        # return "/gea/expedientes/%i" % self.id # MAL: poco portable

    def have_a_deadline(self):
        return False

    def is_nearly_change(self, d=2):
        """
        Return ``True`` if was change is nearly about d, default is 2 days
        """
        return self.modified < timezone.now() - timezone.timedelta(d)
        #return False

    is_nearly_change.boolean = True

    def solicitud_count(self):
        """Devuelve la cantidad de solicitudes que figuran en el expediente."""
        return self.attached_set.filter().count()

    def save(self):
        if(not self.name):
            # esto es porque tiene expediente pero no se ha respondido
            self.name = "Exp" + getDateDay(0)

        super(Expedient, self).save()


    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return str(self.name)

class Collection(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    bank = models.CharField("Banco", max_length=200,choices=BANCOS, default='')
    value = models.DecimalField("Valor", max_digits=15, decimal_places=2, default='0,00')
    createDate = models.DateTimeField("Fecha de creacion", auto_now_add=True)
    name = models.CharField("nombre", max_length=200, blank=True, null=True, )
    description = models.TextField("descripcion", max_length=200, blank=True, null=True, )

    def get_absolute_url(self):
        return reverse('expediente:collection', kwargs={'id': self.id})
        #return reverse('expedientes', kwargs={'id': str(self.id)})
        # return "/gea/expedientes/%i" % self.id # MAL: poco portable

    def save(self):
        if(not self.name):
            # esto es porque tiene expediente pero no se ha respondido
            self.name = str(self.value) + " de " + self.bank

        super(Collection, self).save()
    def __str__(self):
        return str(self.name)

class Attached(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    typeAttached = models.CharField(max_length=200,choices=ADJUNTOS, default='')
    idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField("nombre", max_length=200, blank=True, null=True, )
    description = models.TextField("descripción", blank=True, null=True, )
    upload = models.FileField(upload_to='uploads/')

    def get_absolute_url(self):
        return reverse('expediente:attached', kwargs={'id': self.id})
        #return reverse('expedientes', kwargs={'id': str(self.id)})
        # return "/gea/expedientes/%i" % self.id # MAL: poco portable

    def save(self):
        if(not self.name):
            # esto es porque tiene expediente pero no se ha respondido
            self.name = self.typeAttached
        super(Attached, self).save()
    def __str__(self):
        return str(self.name)
