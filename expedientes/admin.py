
from django.contrib import admin
#from .models import Expedient
from .models import (Expedient, Collection, Attached,)
from django.contrib.admin import DateFieldListFilter
from solicitud.models import PTT, PTT2
from solicitud.admin import PTT2Inline, PTTInline

from guardian.admin import GuardedModelAdmin



class AttachedInline(admin.StackedInline):
    model = Attached
    extra = 0
    list_display = ('id', 'typeAttached')

class AttachedAdmin(admin.ModelAdmin):
    list_display = ('name', 'typeAttached', 'idExpedient','name', 'description',)
    list_filter = ('typeAttached', 'idExpedient',
        #('createDate', DateFieldListFilter),
    )
    search_fields = ('id', 'idExpedient', 'name', 'description')

admin.site.register(Attached, AttachedAdmin)


class CollectionInline(admin.StackedInline):
    model = Collection
    extra = 0
    list_display = ('id', 'bank', 'value')

class CollectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'idExpedient', 'bank','value',)
    list_filter = ('bank', 'idExpedient',
        #('createDate', DateFieldListFilter),
    )
    search_fields = ('id', 'name', 'idExpedient__id', 'bank')

admin.site.register(Collection, CollectionAdmin)


class ExpedientAdmin(GuardedModelAdmin):
    list_display = ('name', 'modified', 'created',)
    list_filter = (
        ('modified', DateFieldListFilter),
        ('created', DateFieldListFilter),
    )
    search_fields = ('id', 'name',)

    inlines = [PTTInline, PTT2Inline, CollectionInline, AttachedInline]

admin.site.register(Expedient, ExpedientAdmin)
