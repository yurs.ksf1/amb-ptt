from django.contrib import admin

from .models import UserProfile, Petitioner, Employee
# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('id','user', 'numDoc', 'firsrtName', 'lastName', 'address', 'municipality','gender',)
	list_filter = ('municipality','gender',)
	search_fields = ('user__username', 'numDoc', 'firsrtName', 'lastName', 'address',)
	#list_editable = ('address', 'firsrtName', 'lastName',)

admin.site.register(UserProfile, UserProfileAdmin)


class PetitionerAdmin(admin.ModelAdmin):
	list_display = ('id','username',)
	list_filter = ('municipality','gender',)
	search_fields = ('username', 'numDoc', 'first_name', 'last_name', 'email','address','phone')

admin.site.register(Petitioner, PetitionerAdmin)


class EmployeeAdmin(admin.ModelAdmin):
	list_display = ('id','username',)
	list_filter = ('occupation',)
	search_fields = ('username','first_name', 'last_name', 'email', 'occupation', 'phone', )


admin.site.register(Employee, EmployeeAdmin)
