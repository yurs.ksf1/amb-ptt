from django.conf.urls import url
from . import views
from django.views.generic import TemplateView

app_name = 'myprofile'
urlpatterns = [
    url(r'^$',                       views.detail, name='detail'),
    url(r'^(?P<pk>[0-9]+)$',         views.detail_1, name='detail_1'),
    url(r'update/',                  views.update, name='update'),
]
