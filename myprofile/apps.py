from django.apps import AppConfig

#from django.contrib.auth import get_user_model

class MyprofileConfig(AppConfig):
    name = 'myprofile'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('UserProfile'))
 #       registry.register(User)
        from django.contrib.auth.models import User, Group, Permission
        registry.register(User)
        registry.register(Group)
        registry.register(Permission)

 #       registry.register(get_user_model())
