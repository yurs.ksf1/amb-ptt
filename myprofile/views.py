# views.py
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.forms.models import inlineformset_factory
from django.shortcuts import render, HttpResponseRedirect, HttpResponse

# importaciones de paquetes
from common.actions import CommonVerbs
from actstream import action

# importaciones propias del app
from .models import UserProfile
from .forms import UserProfileForm, UserForm

@login_required
def update(request):
    user = User.objects.get(pk=request.user.pk)
    form = UserForm(instance=user)

    userProf = UserProfile.objects.get(user=user)

    ProfileInlineFormset = inlineformset_factory(User, UserProfile, fields=('__all__'))
    formset = ProfileInlineFormset(instance=user)

    if request.method == "POST":
        form = UserForm(request.POST, request.FILES, instance=user)
        formset = ProfileInlineFormset(request.POST, request.FILES, instance=user)

        if form.is_valid():
            created_user = form.save(commit=False)
            formset = ProfileInlineFormset(request.POST, request.FILES, instance=created_user)

            if formset.is_valid():
                action.send(request.user, verb=CommonVerbs("save"), description= "perfil de usuario", action_object=userProf)
                created_user.save()
                formset.save()
                return HttpResponseRedirect('/accounts/profile/')

    action.send(request.user, verb=CommonVerbs("edit"), description= "perfil de usuario", action_object=userProf)

    #action.send(request.user, verb='created model', action_object=model)



    template = "edit.html"
    context = {
        "form": form,
        "formset": formset,
    }
    return render(request, template, context)

@login_required
def detail(request):
    user = User.objects.get(pk=request.user.pk)
    userProf = UserProfile.objects.get(user=user)

    #action.send(request.user, verb=CommonVerbs("see"), description= "perfil de usuario")
    action.send(request.user, verb=CommonVerbs("see"), description= "perfil de usuario", action_object=userProf)

    template = 'detail.html'
    context = {
    'user': user,
    'userProf': userProf
    }

    return render(request, template, context)
    #return HttpResponse(template.render(context, request))

@permission_required('is_superuser')
# por el momento solo le pongo la seguridad a que lo pueda hacer un super usuario
def detail_1(request, pk):
    # toca aplicarle seguridad a esta vista para que solo tenga acceso los administrativos
    user = User.objects.get(pk=pk)
    userProf = UserProfile.objects.get(user=user)

    action.send(request.user, verb=CommonVerbs("see"), description= "perfil de usuario", action_object=userProf)

    template = 'detail.html'
    context = {
    'user': user,
    'userProf': userProf
    }

    return render(request, template, context)
    #return HttpResponse(template.render(context, request))
