# models.py

from actstream import registry
from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.contrib.auth.models import Group, Permission
from guardian.compat import reverse

from django.db.models.signals import post_save
from common.choices import MUNICIPIO, GENERO, CARGOS

from common.models import CapitalizePhrase

"""
fiels of from django.contrib.auth.models import User
class AbstractUser ()
    username
    first_name
    last_name
    email

 boleans
    is_staff
    is_active
"""


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='user')
    numDoc = models.CharField("Numero de Documento", max_length=20, validators=[RegexValidator(r'^\d{1,12}$')], default='')

    firsrtName = models.CharField("Nombres" ,max_length=300, )
    lastName = models.CharField("Apellidos" ,max_length=300, )
    gender = models.CharField("Genero",max_length=50, choices=GENERO, blank=True)

    municipality = models.CharField("Municipio de Residencia",max_length=50, choices=MUNICIPIO, blank=True)
    address = models.CharField("Dirección Residencia" ,max_length=100,  blank=True)
    phone = models.CharField("Número de Teléfono",max_length=20, blank=True)

    def __str__(self):
        #return self.user.username + " " + self.numDoc + " " +self.firsrtName + " " +self.lastName
        return self.user.username

    def get_absolute_url(self):
        return reverse('myprofile:detail_1', kwargs={'pk': self.id})
        #return reverse('myprofile:detail', kwargs={'pk': self.id})
        #return "%i/" % self.id
    def save(self, force_insert=False, force_update=False):
        self.lastName = self.lastName.upper().strip()
        self.firsrtName = CapitalizePhrase(self.firsrtName)
        super(UserProfile, self).save(force_insert, force_update)

        if Group.objects.filter(name = 'solicitantes'):
            if not self.user.groups.filter(name = 'solicitantes'):
                #add
                print("add")
                g_sol = Group.objects.get(name = 'solicitantes')
                self.user.groups.add(g_sol)
            else:
                # nothing
                print("ya esta en el grupo")
        else:
            print("No existe el grupo")

def create_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_profile = UserProfile(user=user)
        user_profile.save()
post_save.connect(create_profile, sender=User)

class CustomUser(User):
    phone = models.CharField("Número de Teléfono",max_length=20, blank=True)

    class Meta:
        abstract = True

class Petitioner(CustomUser):
    numDoc = models.CharField("Numero de Documento", max_length=20, validators=[RegexValidator(r'^\d{1,12}$')], default='')
    gender = models.CharField("Genero",max_length=50, choices=GENERO, blank=True)
    municipality = models.CharField("Municipio de Residencia",max_length=50, choices=MUNICIPIO, blank=True)
    address = models.CharField("Dirección Residencia" ,max_length=100,  blank=True)


class Employee (CustomUser):
    occupation = models.CharField("Cargo laboral" ,max_length=100, choices=CARGOS,  blank=True)
