from django.db import models
import uuid
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from common.choices import COORDINACIONES, MUNICIPIOS, SOLICITUDES, BANCOS, ADJUNTOS, TERRITORIOS, JUSTIFICACIONES, ALTURAS
from django.urls import reverse
from myprofile.models import UserProfile
from datetime import timezone, datetime, timedelta, date
from django.db.models.signals import post_save


from django.utils import timezone

from django_extensions.db.models import TimeStampedModel
# Create your models here.

class Expedient(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    def __str__(self):
        return str(self.id)
    #self.update_modified = kwargs.pop('update_modified', getattr(self, 'update_modified', True))
    #super(TimeStampedModel, self).save(**kwargs)


class Evidence(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    typeEvidence = models.CharField("Tipo de Evidencia", max_length=100,  blank=True, null=True,)
    def have_a_deadline1(self):
        """
        Return ``True`` if this deadline is in the future.
        """
        #return self.deadline > timezone.now()
        return False

    def have_a_deadline(self):
        """Devuelve la cantidad de personas que figuran como propietarias."""
        return self.deadline_set.filter(isDone=False).count()

    have_a_deadline.boolean = False

    def __str__(self):
        return str(self.id)

    class Meta:
        #abstract = True
        pass

# --- common ---
class Deadline(models.Model):
    #objects = DeadlineManager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    idEvidence = models.ForeignKey(Evidence, blank=True, null=True, on_delete=models.CASCADE)
    deadline = models.DateTimeField("Fecha limite", help_text='The time of the deadline.')
    text = models.TextField("Descipcion", blank=True, null=True, help_text='An optional text to show.')
    isDone = models.BooleanField(default=False)

    class Meta:
        #app_label = 'core'
        verbose_name = 'Deadline'
        verbose_name_plural = 'Deadlines'
        ordering = ['-deadline']
        #abstract = True

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return unicode(self.deadline)

    def __repr__(self):
        return 'Deadline(id={id}, deadline={deadline})'.format(**self.__dict__)

    def can_delete(self, user_obj):
        if self.id is None:
            return False
        if user_obj.is_superuser:
            return True
        else:
            return False

    def is_in_the_future(self):
        """
        Return ``True`` if this deadline is in the future.
        """
        return self.deadline > timezone.now()

    is_in_the_future.boolean = True


    def is_nearly(self, d=2):
        """
        Return ``True`` if this deadline is in the future and is nearly about d, default is 2 days
        """
        return (self.deadline < timezone.now() + timezone.timedelta(d) ) and (self.deadline > timezone.now())

    is_nearly.boolean = True


    def is_done(self):
        """
        Return ``True`` if this deadline is in the future and is nearly about d, default is 2 days
        """
        return self.isDone

    def is_in_the_past(self):
        """
        Return ``True`` if this deadline is in the past.
        """
        return self.deadline < timezone.now()

    def has_text(self):
        """
        Checks that the text is not ``None`` or an empty string.
        """
        if self.text is None:
            return False
        else:
            return self.text.strip() != ''

# --- type evidence ---
class Collection(Evidence):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    #idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    bank = models.CharField("Banco", max_length=200,choices=BANCOS, default='')
    value = models.DecimalField("Valor", max_digits=15, decimal_places=2, default='0,00')
    #createDate = models.DateTimeField("Fecha de creacion", auto_now_add=True)

    def save(self):
        if(not self.typeEvidence):
            # esto es porque tiene expediente pero no se ha respondido
            self.typeEvidence = 'Collection'
        super(Collection, self).save()
        
    def __str__(self):
        return str(self.id)

class Attached(Evidence):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    typeAttached = models.CharField(max_length=200,choices=ADJUNTOS, default='')
    #idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField("nombre", max_length=200)
    description = models.TextField("descripción")
    #anexo = FilerFileField(null=True)

    def save(self):
        if(not self.typeEvidence):
            # esto es porque tiene expediente pero no se ha respondido
            self.typeEvidence = 'Attached'
        super(Attached, self).save()

    def __str__(self):
        return str(self.id)

class Request(Evidence):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(max_length=200, unique=True)  # Agregar en el modelo!!! Importante!!!!

    #idExpedient = models.ForeignKey(Expedient, blank=True, null=True, on_delete=models.CASCADE)
    #petitioner = models.ForeignKey("Solicitante", UserProfile, blank=True, null=True)
    typeRequest =models.CharField("Tipo de solicitud", max_length=100,choices=SOLICITUDES)  # esto puede ser de otro modelo, y que este modelo sea abstracto y se agregen cosas o algo asi... jaja
    address = models.CharField("Dirección", max_length=100, default=' ')
    neighborhood = models.CharField("Barrio", max_length=100, default=' ')
    municipality = models.CharField("Municipio", max_length=100,choices=MUNICIPIOS, default=' ')

    #createDate = models.DateField("Fecha de creacion", default=date.today)  # Agregar!
    #answerDate = models.DateField("Fecha estimada de Respuesta", default=datetime.now()+timedelta(days=18))
    answerd = models.BooleanField(default=False)
    approved = models.BooleanField(default=False)

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('solicitud:details', kwargs={'slug': self.slug})

    class Meta:
        permissions = (
            ('view_request', 'Can view request'),
        )
        verbose_name = ("Solicitud")
        verbose_name_plural = ("Solicitudes")
        abstract = True

def create_expediente(sender, **kwargs):
    exp = kwargs["instance"]

    #print(exp)
    if kwargs["created"]:
        if exp.idExpedient:
            print("Si")
        else:
            print("No")
            my_exp = Expedient()
            my_exp.save()
            exp.idExpedient = my_exp
            exp.save()
    else:
        # asumo que es una modificacion -- si ya se que no se debe asumir..
        print("3")
        if exp.idExpedient:
            print("Si")
            exp.idExpedient.save()
        else:
            print("wft - deberia tener exp")

# --- type of request
class PTT(Request):
    space = models.CharField("Territorio", max_length=100,choices=TERRITORIOS)
    motive = models.CharField("Motivo", max_length=100,choices=JUSTIFICACIONES)
    height = models.CharField("Altura", max_length=100,choices=ALTURAS)

    class Meta:
        permissions = (
            ('view_ptt', 'Can view ptt'),
        )
        get_latest_by = 'createDate'
        verbose_name = ("PTT")
        verbose_name_plural = ("PTT")


    def __unicode__(self):
        return self.title

    #def get_absolute_url(self):
    #    return reverse('solicitud:details', kwargs={'slug': self.slug})

    def save(self):
        if(not self.typeEvidence):
            # esto es porque tiene expediente pero no se ha respondido
            self.typeEvidence = 'Request'
        super(PTT, self).save()

#post_save.connect(create_expediente, sender=PTT)
#post_save.connect(create_expediente, sender=Attached)
#post_save.connect(create_expediente, sender=Collection)
