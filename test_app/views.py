from django.shortcuts import render

import logging


logger = logging.getLogger(__name__)

def index(request):
	msj = "Hello index desde test_app"
	template = 'index.html'
	context = {'msj': msj,}	
	logger.debug("Hello index desde test_app")
	return render(request, template, context)



def logging_example(request):
	"""logging example
	"""
	logger.debug('this is DEBUG message.')
	logger.info('this is INFO message.')
	logger.warning('this is WARNING message.')
	logger.error('this is ERROR message.')
	logger.critical('this is CRITICAL message.')

	msj = "Hello logging_example desde test_app"
	template = 'index.html'
	context = {'msj': msj,}	
	return render(request, template, context)