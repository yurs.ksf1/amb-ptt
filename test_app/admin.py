
from django.contrib import admin
#from .models import Expedient
from .models import Expedient, Collection, Attached, Evidence, Deadline, PTT
from django.contrib.admin import DateFieldListFilter
#from solicitud.models import PTT


from guardian.admin import GuardedModelAdmin


class DeadlineAdmin(admin.ModelAdmin):
    list_display = ('id', 'deadline', 'text', 'isDone', 'is_in_the_future', 'is_nearly')
    list_filter = ('idEvidence__typeEvidence', 'isDone')
    search_fields = ('id', 'deadline', 'text', 'idEvidence__typeEvidence')

    #list_display_links = ('is_in_the_future', 'is_nearly')
    #pass
    #date_hierarchy = 'createDate'
    #list_display = ('createDate', 'modifyDate',)
    #readonly_fields = list_display

class DeadlineInline(admin.StackedInline):
    model = Deadline
    extra = 0
    list_display = ('id')

admin.site.register(Deadline,   DeadlineAdmin)


class EvidenceAdmin(GuardedModelAdmin):
    list_display = ('id', 'idExpedient', 'modified', 'created','have_a_deadline')
    list_filter = (
        ('modified', DateFieldListFilter),
        ('created', DateFieldListFilter),
    )
    search_fields = ('id',)

    inlines = [DeadlineInline]

class EvidenceInline(admin.StackedInline):
    model = Evidence
    extra = 0
    list_display = ('id')

#admin.site.register(Evidence,   EvidenceAdmin)


class AttachedInline(admin.StackedInline):
    model = Attached
    extra = 0
    list_display = ('id', 'typeAttached')

class AttachedAdmin(admin.ModelAdmin):
    list_display = ('id', 'typeAttached', 'idExpedient','name', 'description',)
    list_filter = ('typeAttached', 'idExpedient',
        #('createDate', DateFieldListFilter),
    )
    search_fields = ('id', 'idExpedient', 'name', 'description')
    inlines = [DeadlineInline]

admin.site.register(Attached,   AttachedAdmin)


class CollectionInline(admin.StackedInline):
    model = Collection
    extra = 0
    list_display = ('id', 'bank', 'value')

class CollectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'idExpedient', 'bank','value','have_a_deadline')
    list_filter = ('bank', 'idExpedient',
        #('createDate', DateFieldListFilter),
    )
    search_fields = ('id', 'idExpedient__id', 'bank')
    inlines = [DeadlineInline]

admin.site.register(Collection, CollectionAdmin)


class PTTInline(admin.StackedInline):
    model = PTT
    extra = 0
    list_display = ('id', 'create', 'approved')
    #list_filter = ('categoria', 'usuario',)
    #search_fields = ('categoria__titulo', 'usuario__email')

class PTTAdmin(admin.ModelAdmin):
    list_display = ('id',  'idExpedient' , 'approved')
    list_filter = ('idExpedient',
        #('createDate', DateFieldListFilter),
    )
    search_fields = ('id', 'idExpedient__id', 'bank')
    inlines = [DeadlineInline]

admin.site.register(PTT,   PTTAdmin)


class ExpedientAdmin(GuardedModelAdmin):
    list_display = ('id', 'modified', 'created',)
    list_filter = (
        ('modified', DateFieldListFilter),
        ('created', DateFieldListFilter),
    )
    search_fields = ('id',)

    inlines = [CollectionInline, AttachedInline, PTTInline]

    pass
    #list_display = ('title', 'slug', 'created_at')
    #list_filter = ('created_at',)
    #search_fields = ('title',)

admin.site.register(Expedient,  ExpedientAdmin)




#admin.site.register(Expedient)
#admin.site.register(petitioner)



#admin.site.register(Informe_tecnico)
#admin.site.register(Ficha_individuo)
#admin.site.register(AgendarVisita)
#admin.site.register(Visita)
#admin.site.register(Seguimiento)
#admin.site.register(Acta)
#admin.site.register(Respuesta)
